package com.abc.notification;

public class GoldPlan implements Plan {

	private MailNotificationVendor mailProvider;
	private SMSNotificationVendor smsProvider;

	public GoldPlan(final MailNotificationVendor mailProvider, final SMSNotificationVendor smsProvider) {
		this.mailProvider = mailProvider;
		this.smsProvider = smsProvider;
	}

	@Override
	public void sendNotification(Recipeint recipeint,int notificationId,NotificationRequest payload) {
		mailProvider.sendMessage(recipeint);
		smsProvider.sendMessage(recipeint);

	}

}
