package com.abc.notification;

import java.util.List;

//noticn engine 
//
//silver gld            diamond 
//sms    sms email     +push noti

public class Notification {

	private int notificationId;
	private NotificationRequest payload;
	private List<Recipeint> recipents;
	
	

	/**
	 * @param notificationId
	 * @param payload
	 * @param recipents
	 */
	public Notification(int notificationId, NotificationRequest payload, List<Recipeint> recipents) {
 		this.notificationId = notificationId;
		this.payload = payload;
		this.recipents = recipents;
	}



	public void sendMessage() {
		this.recipents.forEach(rec -> rec.getPlan().sendNotification(rec, notificationId, payload));

	}

}
