package com.abc.notification;

public class Recipeint {

	int userId;
	String userName;
	String email;
	int mobNo;
	String address;
	Plan plan;

	/**
	 * @param userId
	 * @param userName
	 * @param email
	 * @param mobNo
	 * @param address
	 */
	public Recipeint(int userId, String userName, String email, int mobNo, String address, Plan plan) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.email = email;
		this.mobNo = mobNo;
		this.address = address;
		this.plan = plan;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the mobNo
	 */
	public int getMobNo() {
		return mobNo;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return the planType
	 */
	public Plan getPlan() {
		return plan;
	}

}
