package com.abc.notification;

public class DiamondPlan implements Plan {

	private MailNotificationVendor mailProvider;
	private SMSNotificationVendor smsProvider;
	private PUSHNotificationVendor pushProvider;

	public DiamondPlan(final MailNotificationVendor mailProvider, final SMSNotificationVendor smsProvider,
			final PUSHNotificationVendor pushProvider) {
		this.mailProvider = mailProvider;
		this.smsProvider = smsProvider;
		this.pushProvider = pushProvider;
	}

	@Override
	public void sendNotification(Recipeint recipeint,int notificationId,NotificationRequest payload) {
		mailProvider.sendMessage(recipeint);
		smsProvider.sendMessage(recipeint);
		pushProvider.sendMessage(recipeint);

	}

}
