package com.abc.notification;

public class NotificationRequest {
	
	
	private String messageBody;

	/**
	 * @param messageBody
	 */
	public NotificationRequest(String messageBody) {
		super();
		this.messageBody = messageBody;
	}

	/**
	 * @return the messageBody
	 */
	public String getMessageBody() {
		return messageBody;
	}
	

 
}
