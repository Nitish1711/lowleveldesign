package com.abc.notification;

public class SilverPlan implements Plan {

	private MailNotificationVendor mailProvider;

	public SilverPlan(final MailNotificationVendor mailProvider) {
		this.mailProvider = mailProvider;
 	}

	@Override
	public void sendNotification(Recipeint recipeint,int notificationId,NotificationRequest payload) {
		mailProvider.sendMessage(recipeint);
 
	}

}
