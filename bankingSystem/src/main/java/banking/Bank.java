package banking;

import java.util.LinkedHashMap;
import java.util.Random;

/**
 * Private Variables:<br>
 * {@link #accounts}: List&lt;Long, Account&gt;
 */
public class Bank implements BankInterface {
	private LinkedHashMap<Long, Account> accounts;
	private Random random;

	public Bank() {
		accounts=new LinkedHashMap<Long, Account>();
		random=new Random();
		
	}

	protected Account getAccount(Long accountNumber) {
		 return accounts.get(accountNumber);
	}

	public Long openCommercialAccount(Company company, int pin, double startingDeposit) {
		Long genertedAccNo=random.nextLong();
		CommercialAccount companyAccount=new CommercialAccount(company, genertedAccNo, pin, startingDeposit);
		accounts.put(genertedAccNo, companyAccount);
		return genertedAccNo;
	}

	public Long openConsumerAccount(Person person, int pin, double startingDeposit) {
		Long genertedAccNo=random.nextLong();
		ConsumerAccount consumerAcc=new ConsumerAccount(person, genertedAccNo, pin, startingDeposit);
		accounts.put(genertedAccNo, consumerAcc);
		return genertedAccNo;
	}

	public boolean authenticateUser(Long accountNumber, int pin) {
		Account account=accounts.get(accountNumber);
		return account.validatePin(pin);
	}

	public double getBalance(Long accountNumber) {
		Account account=accounts.get(accountNumber);
		return account.getBalance();
	}

	public void credit(Long accountNumber, double amount) {
		Account account=accounts.get(accountNumber);  
		account.creditAccount(amount);

	}

	public boolean debit(Long accountNumber, double amount) {
		Account account=accounts.get(accountNumber);  
		return account.debitAccount(amount);
	}
}
