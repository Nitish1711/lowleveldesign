package banking;

public class Company extends AccountHolder {
	public Company(String companyName, int taxId) {
		super(taxId);
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	private String companyName;

}
