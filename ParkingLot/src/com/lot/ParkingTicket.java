/**
 * 
 */
package com.lot;

/**
 * @author nitkulsh
 *
 */
public class ParkingTicket {
	
	private Vehicle vehicle;
	
	private int parkedAt;
	
	private Slot slot;
	
	private long transactionId;
	
	

	public ParkingTicket(Vehicle vehicle, int parkedAt, Slot slot, long transactionId) {
		this.vehicle = vehicle;
		this.parkedAt = parkedAt;
		this.slot = slot;
		this.transactionId = transactionId;
	}

	/**
	 * @return the vehicle
	 */
	public Vehicle getVehicle() {
		return vehicle;
	}

	/**
	 * @return the parkedAt
	 */
	public int getParkedAt() {
		return parkedAt;
	}

	/**
	 * @return the slot
	 */
	public Slot getSlot() {
		return slot;
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	@Override
	public String toString() {
		return "ParkingTicket [vehicle=" + vehicle + ", parkedAt=" + parkedAt + ", slot=" + slot + ", transactionId="
				+ transactionId + "]";
	}

}
