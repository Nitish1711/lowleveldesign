package com.lot;

public class Manager {
	
	/*
	 * The functions that the parking lot system can do: Create the parking lot. Add
	 * floors to the parking lot. Add a parking lot slot to any of the floors. Given
	 * a vehicle, it finds the first available slot, books it, creates a ticket,
	 * parks the vehicle, and finally returns the ticket. Unparks a vehicle given
	 * the ticket id. Displays the number of free slots per floor for a specific
	 * vehicle type. Displays all the free slots per floor for a specific vehicle
	 * type. Displays all the occupied slots per floor
	 */


	public static void main(String[] args) {
		
		ParkingLot parkingLot=new ParkingLot("METRO", "DELHI");
		Floor floor1=new Floor(1);
		Floor floor2=new Floor(2);
		
		Slot floor1Slot1= new Slot("1A", SlotType.CAR, false, 1);
		Slot floor1Slot2= new Slot("1C", SlotType.BIKE, false, 1);

		Slot floor2Slot1= new Slot("2A", SlotType.BUS, false, 1);
		Slot floor2Slot2= new Slot("2D", SlotType.BIKE, false, 1);

		
		floor1.addSlot(floor1Slot1);
		floor1.addSlot(floor1Slot2);
		
		floor2.addSlot(floor2Slot1);
		floor2.addSlot(floor2Slot2);

		
		parkingLot.addFloor(floor1);
		parkingLot.addFloor(floor2);
		
		Long totalFreeSlotsFloor1=parkingLot.getAllFreeSlotsIdsForAFloor(1, SlotType.CAR);
		Long totalFreeSlotsFloor2=parkingLot.getAllFreeSlotsIdsForAFloor(2, SlotType.CAR);
		System.out.println("Total Free car Slots at 1st Floor "+totalFreeSlotsFloor1);
		System.out.println("Total Free car Slots at 2st Floor "+totalFreeSlotsFloor2);

		
		Vehicle vehicle1=new Vehicle(1, VehicleType.CAR, "UPXX123");
		
		Vehicle vehicle2=new Vehicle(2, VehicleType.CAR, "UPXX121");
		
		ParkingTicket tikcet=parkingLot.parkVehicle(vehicle1);
		System.out.println(vehicle1.getVehicleId()+" parking ticket "+tikcet.toString());
		
		System.out.println("Total Free car Slots at 1st Floor "+totalFreeSlotsFloor1);
		System.out.println("Total Free car Slots at 2st Floor "+totalFreeSlotsFloor2);

		parkingLot.parkVehicle(vehicle2);
		System.out.println("Total Free car Slots at 1st Floor "+totalFreeSlotsFloor1);
		System.out.println("Total Free car Slots at 2st Floor "+totalFreeSlotsFloor2);

		parkingLot.unParkVehicle(tikcet);
 
		System.out.println("Total Free car Slots at 1st Floor "+parkingLot.getAllFreeSlotsIdsForAFloor(1, SlotType.CAR));
		System.out.println("Total Free car Slots at 2st Floor "+parkingLot.getAllFreeSlotsIdsForAFloor(2, SlotType.CAR));


 
 
		

	}

}
