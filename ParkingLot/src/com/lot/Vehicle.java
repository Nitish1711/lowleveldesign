/**
 * 
 */
package com.lot;

/**
 * @author nitkulsh
 *
 */
public class Vehicle {
	
	private final int vehicleId;
	
	private final VehicleType vehicleType;
	private final String vehicleNumber;

	
	public Vehicle(int vehicleId, VehicleType vehicleType, String vehicleNumber) {
		super();
		this.vehicleId = vehicleId;
		this.vehicleType = vehicleType;
		this.vehicleNumber = vehicleNumber;
	}


	/**
	 * @return the vehicleId
	 */
	public int getVehicleId() {
		return vehicleId;
	}


	/**
	 * @return the vehicleType
	 */
	public VehicleType getVehicleType() {
		return vehicleType;
	}


	/**
	 * @return the vehicleNumber
	 */
	public String getVehicleNumber() {
		return vehicleNumber;
	}


}
