package com.lot;

public class Slot {

	private String slotId;
	private SlotType slotType;
	private boolean booked;
	private int floorId;

	public Slot(String slotId, SlotType slotType, boolean booked,final int floorId) {
		this.slotId = slotId;
		this.slotType = slotType;
		this.booked = false;
		this.floorId=floorId;
	}

	public void markSlotBooked() {
		this.booked = true;
	}
	
	
	public void markSlotAvailable() {
		this.booked = false;
	}


	/**
	 * @return the slotId
	 */
	public String getSlotId() {
		return slotId;
	}

	/**
	 * @return the slotType
	 */
	public SlotType getSlotType() {
		return slotType;
	}

	/**
	 * @return the booked
	 */
	public boolean isBooked() {
		return booked;
	}

	/**
	 * @return the floorId
	 */
	public int getFloorId() {
		return floorId;
	}

}
