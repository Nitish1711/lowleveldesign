/**
 * 
 */
package com.lot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author nitkulsh
 *
 */

/*
 * The functions that the parking lot system can do: Create the parking lot. Add
 * floors to the parking lot. Add a parking lot slot to any of the floors. Given
 * a vehicle, it finds the first available slot, books it, creates a ticket,
 * parks the vehicle, and finally returns the ticket. Unparks a vehicle given
 * the ticket id. Displays the number of free slots per floor for a specific
 * vehicle type. Displays all the free slots per floor for a specific vehicle
 * type. Displays all the occupied slots per floor
 */

public class ParkingLot {

	private List<Floor> floors;
	private final String parkingLotName;
	private final String city;

	private int uuid = 0;

	private Map<Integer, List<Slot>> slotsPerFloor;

	public ParkingLot(final String parkingLotName, final String city) {
		this.parkingLotName = parkingLotName;
		this.city = city;
		this.floors = new ArrayList<Floor>();
		this.slotsPerFloor = new HashMap<Integer, List<Slot>>();
	}

	public void addFloor(final Floor newFloor) {
		this.getFloors().add(newFloor);
		this.slotsPerFloor.put(newFloor.getFloorNo(), newFloor.getSlots());
	}

	public ParkingTicket parkVehicle(Vehicle vehicle) {
		Optional<Slot> availableSlot = findNextAvailableSlot(vehicle.getVehicleType());
		if (availableSlot.isPresent()) {
			availableSlot.get().markSlotBooked();
			return generateTicket(vehicle, availableSlot.get());
		}
		return null;
	}

	public void unParkVehicle(ParkingTicket ticket) {
		ticket.getSlot().markSlotAvailable();
	}

	private ParkingTicket generateTicket(Vehicle vehicle, Slot slot) {
		ParkingTicket parkingTicket = new ParkingTicket(vehicle, slot.getFloorId(), slot, ++uuid);
		return parkingTicket;
	}

	private Optional<Slot> findNextAvailableSlot(VehicleType type) {
		List<Integer> freeFloors = new ArrayList<Integer>(slotsPerFloor.keySet());
		freeFloors.sort(Comparator.reverseOrder());
		for (Integer floorId : freeFloors) {
			for (Slot slot : slotsPerFloor.get(floorId)) {
				if (slot.getSlotType().toString().toUpperCase().equals(type.toString()) && !slot.isBooked())
					return Optional.of(slot);

			}

		}

		return Optional.empty();
	}

	public List<Slot> getAllFreeSlotsOfAFloor(int floorId, SlotType slotType) {
		List<Slot> slotsGivenFloor = slotsPerFloor.get(floorId);
		List<Slot> freeSlots = new ArrayList<Slot>();
		for (Slot slot : slotsGivenFloor) {
			if (slot.getSlotType().toString().toUpperCase().equals(slotType.toString()) && !slot.isBooked())
				freeSlots.add(slot);
		}
		return freeSlots;
	}

	public List<Slot> getAllOccupiedSlotsOfAFloor(int floorId, SlotType slotType) {
		List<Slot> slotsGivenFloor = slotsPerFloor.get(floorId);

		List<Slot> occpiedSlots = new ArrayList<Slot>();
		for (Slot slot : slotsGivenFloor) {
			if (slot.getSlotType().toString().toUpperCase().equals(slotType.toString()) && slot.isBooked())
				occpiedSlots.add(slot);
		}
		return occpiedSlots;
	}

	public Long getAllFreeSlotsIdsForAFloor(int floorId, SlotType slotType) {
		List<Slot> slotsGivenFloor = slotsPerFloor.get(floorId);
		long count = 0;
		for (Slot slot : slotsGivenFloor) {
			if (slot.getSlotType().toString().toUpperCase().equals(slotType.toString()) && !slot.isBooked())
				count++;
		}

		return count;

	}

	/**
	 * @return the floors
	 */
	public List<Floor> getFloors() {
		return floors;
	}

	/**
	 * @return the parkingLotName
	 */
	public String getParkingLotName() {
		return parkingLotName;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

}
