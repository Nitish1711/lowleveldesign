package com.lot;

import java.util.ArrayList;
import java.util.List;

public class Floor {

	private final int floorNo;

	private List<Slot> slots;

	public void addSlot(final Slot newSlot) {
		this.getSlots().add(newSlot);
	}
	
	

	public Floor(final int floorNo) {
		this.floorNo = floorNo;
		this.slots = new ArrayList<Slot>();
	}

	/**
	 * @return the slots
	 */
	public List<Slot> getSlots() {
		return slots;
	}

	/**
	 * @return the floorNo
	 */
	public int getFloorNo() {
		return floorNo;
	}

}
