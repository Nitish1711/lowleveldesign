package com.treebo;

import java.util.ArrayList;
import java.util.List;

public class Floor {

	int floorNo;
	List<Room> rooms;

	public Floor(int floorNo, List<Room> rooms) {
		this.floorNo = floorNo;
		this.rooms = new ArrayList<Room>();
	}

	/**
	 * @return the floorNo
	 */
	public int getFloorNo() {
		return floorNo;
	}
	/**
	 * @return the rooms
	 */
	public List<Room> getRooms() {
		return rooms;
	}

}
