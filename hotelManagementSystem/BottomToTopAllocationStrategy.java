package com.treebo;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class BottomToTopAllocationStrategy implements AllocationStrategy {

	@Override
	public Optional<Room> allocateRoom(List<Floor> floors) {
		Collections.sort(floors, (o1, o2) -> { // asc order top to bottom
			if (o1.getFloorNo() > o2.getFloorNo())
				return 1;
			else
				return -1;

		});
		for (Floor floor : floors) {
			Optional<Room> roomOpt = floor.getRooms().stream().filter(rm -> rm.isbooked()==false).findFirst();
			roomOpt.get().markAsBooked();
			return roomOpt;

		}
		return Optional.empty();
	}

}
