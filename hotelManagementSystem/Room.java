package com.treebo;

public class Room {

	int roomNo;
	int floorNo;
	boolean booked;

	/**
	 * @param roomNo
	 * @param floorNo
	 */
	public Room(int roomNo, int floorNo) {
		this.roomNo = roomNo;
		this.floorNo = floorNo;
		booked = false;
	}

	public void markAsBooked() {
		this.booked = true;
	}

	public void markAsAvailable() {
		this.booked = false;
	}

	/**
	 * @return the roomNo
	 */
	public int getRoomNo() {
		return roomNo;
	}

	/**
	 * @return the floorNo
	 */
	public int getFloorNo() {
		return floorNo;
	}

	/**
	 * @return the isbooked
	 */
	public boolean isbooked() {
		return booked;
	}

}
