package com.treebo;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RunnerTest {
	HotelAdmin hotelAdmin;

	@BeforeEach
	void setUp() {

		AllocationStrategy allocationStrategy = new TopToBottomAllocationStrategy();
		List<Floor> floors = new ArrayList<Floor>();
		floors.add(new Floor(1, new ArrayList<Room>()));
		floors.add(new Floor(2, new ArrayList<Room>()));
		floors.add(new Floor(3, new ArrayList<Room>()));
		
		hotelAdmin = new HotelAdmin(allocationStrategy, floors, "TAJ");
		hotelAdmin.addRoom(floors.get(0), "TAJ");
		hotelAdmin.addRoom(floors.get(0), "TAJ");
		hotelAdmin.addRoom(floors.get(0), "TAJ");
		hotelAdmin.addRoom(floors.get(0), "TAJ");

		hotelAdmin.addRoom(floors.get(1), "TAJ");
		hotelAdmin.addRoom(floors.get(1), "TAJ");
		hotelAdmin.addRoom(floors.get(1), "TAJ");
		hotelAdmin.addRoom(floors.get(1), "TAJ");
		
		hotelAdmin.addRoom(floors.get(2), "TAJ");
		hotelAdmin.addRoom(floors.get(2), "TAJ");
		hotelAdmin.addRoom(floors.get(2), "TAJ");
		hotelAdmin.addRoom(floors.get(2), "TAJ");

	}

	@Test
	void testCheckinFlow() {
		List<Integer> roomsAlloted = hotelAdmin.checkInRooms(5);
		roomsAlloted.forEach(room -> System.out.println("ROOM ALLOTED :" + room));
	}

	@Test
	void testCheckOutFlow() {
		List<Integer> roomsToCheckOut = new ArrayList<Integer>();
		roomsToCheckOut.add(10);
		roomsToCheckOut.add(11);
		hotelAdmin.checkoutRooms(roomsToCheckOut);

	}

}
