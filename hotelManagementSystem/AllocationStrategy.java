package com.treebo;

import java.util.List;
import java.util.Optional;

public interface AllocationStrategy {
	
	public Optional<Room> allocateRoom(List<Floor> floors);

}
