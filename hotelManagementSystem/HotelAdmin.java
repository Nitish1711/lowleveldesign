package com.treebo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HotelAdmin {
	
	private AllocationStrategy allocationStrategy;
	
	private List<Floor> floors;
	
	private String hotelName; // hotel name for reference
	
	private HashMap<Integer, Room> hotelRooms;
		
	
	public HotelAdmin(AllocationStrategy allocationStrategy,List<Floor> floors,String hotelName) {
		this.allocationStrategy=allocationStrategy;
		this.floors=new ArrayList<Floor>();
		this.floors.addAll(floors);
		this.hotelName=hotelName;
		hotelRooms=new HashMap<Integer, Room>();
	}
	
	
	public void addRoom(Floor floor, String hotelName) {
		Room room = null;
		if (hotelRooms.size() == 0) {
			room = new Room(1, floor.getFloorNo());
		} else {
			int maxKey = hotelRooms.entrySet().stream()
					.max((entry1, entry2) -> entry1.getKey() > entry2.getKey() ? 1 : -1).get().getKey();
			room = new Room(++maxKey, floor.getFloorNo());
		}
		
		for(Floor flr:this.floors) {
			if(floor.getFloorNo()==flr.getFloorNo()) {
				flr.getRooms().add(room);
				break;
			}
		}
   		
  		
		hotelRooms.putIfAbsent(room.getRoomNo(), room);
	}	
	
	
	
	public List<Integer> checkInRooms(int noOfRooms) {
		List<Integer> roomsAssigned = new ArrayList<Integer>();
		for (int i = 1; i <= noOfRooms; i++) {
			roomsAssigned.add(this.allocationStrategy.allocateRoom(this.floors).get().getRoomNo());
		}

		return roomsAssigned;
	}
	
	public void checkoutRooms(List<Integer> roomsToVacate) {
		for (Integer roomNo : roomsToVacate) {
			hotelRooms.get(roomNo).markAsAvailable();
			System.out.println("ROOM NO "+roomNo+" BOOKED STATUS  "+hotelRooms.get(roomNo).isbooked());
		}
	}	

	

}
